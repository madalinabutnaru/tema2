import React from 'react';

export class AddProduct extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            id: 0,
            productName: "",
            price: 0
        };

    }
    
    clearFields = () => {
        this.setState({
           id: 0,
            productName: "",
            price: 0
        })
    }
    
    handleChangeProductName = (e) => {
        this.setState({
           productName: e.target.value
        });
        
    }
    
    handleChangePrice = (e) => {
        this.setState({
            price: e.target.value
        });
        
    }
    
    handleChangeId = (e) => {
        this.setState({
            id: e.target.value
        });
        
    }
    
    onItemAdded = () => {
      let product = {
          productName: this.state.productName,
          price: this.state.price,
          id: this.state.id
      }
      this.props.handleAdd(product);
      this.clearFields()
    }
    
    render(){
        return (
            <React.Fragment>
                <div>
                    <h1>Add Product </h1>
                    <div>
                        <input type="text" placeholder="Product Name" value={this.state.productName} onChange={this.handleChangeProductName} />
                        <input type="text" placeholder="Price" value={this.state.price} onChange={this.handleChangePrice}/>
                        <input type="number" value={this.state.id}  onChange={this.handleChangeId}/>
                        <button onClick={this.onItemAdded}>Add Product</button>
                    </div>
                </div>
            </React.Fragment>
            )
    }
}