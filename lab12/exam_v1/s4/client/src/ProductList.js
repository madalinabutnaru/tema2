import React from 'react';


export class ProductList extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            source: []
        };
    }
    
    render(){
        let items = this.props.source.map((item, index) => 
            <div key={index}>{item.productName}  {item.price}</div>
        );
        return (
            <React.Fragment>
            <div>
                <h1>{this.props.title}</h1>
                <div className="items-container">
                {items}
                </div>
            </div>
            </React.Fragment>
)    
    }
    
}